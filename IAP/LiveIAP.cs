using UnityEngine;
using UnityEngine.Purchasing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class LiveIAP : MonoBehaviour, IStoreListener
{
    public ARObjectSettingsScriptable _IAPSettings;
    public static LiveIAP Instance;
    public static IStoreController _storeController;
    public static IExtensionProvider _storeExtentionProvider;
    public IStoreListener _StoreLisitener;
    public ProductCatalog _catalog;
    public static bool isInitialized;

    public static Action<Product> OnItemPurchasedSucess;
    public static Action<Product> OnItemPurchasedFailed;
    /// <summary>
    /// is locally init 
    /// </summary>
    public static Action<bool> OnInit;
    public static Action OnInitLocally;



    private void Awake()
    {
        _catalog = ProductCatalog.LoadDefaultCatalog();
        Instance = this;

    }
    public void Start()
    {

        Invoke(nameof(InitIAP), 3);
        /* foreach (var item in _catalog.allProducts)
         {
             Debug.Log("CAT==" + item.re);
             Debug.Log("PRICE1==" + item.udpPrice.value);
             Debug.Log("PRICE2==" + item.googlePrice.value);
             Debug.Log("PRICE3==" + item.applePriceTier);
         }*/
        Debug.LogError("A");

    }
    private void OnEnable()
    {

    }
    private void OnDisable()
    {

    }
    public void InitIAP()
    {
        UIManager.GetLoadingUI().Show();
        _catalog = ProductCatalog.LoadDefaultCatalog();

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        // you can add products manually here
        foreach (var item in _catalog.allProducts)
        {
            builder.AddProduct(item.id, ProductType.NonConsumable);
        }
        UnityPurchasing.Initialize(this, builder);

        var allprodcutList = _catalog.allProducts.ToList();
        /* for (int i = 0; i < _catalog.allProducts.Count; i++)
         {
             _IAPSettings._ProdcutItems[i].ItemProduct = allprodcutList[i];
         }*/
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        isInitialized = false;
        Debug.LogError("IAP_INIT_FAILED:" + error.ToString());
        LoadIAPLocally();
        UIManager.GetLoadingUI().Hide();
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
    {
        Debug.Log("IAP_PRPOCESSPURCHASE:" + purchaseEvent.purchasedProduct.receipt);
        OnItemPurchasedSucess?.Invoke(purchaseEvent.purchasedProduct);
        SaveIAP(purchaseEvent.purchasedProduct.definition.id);
        return PurchaseProcessingResult.Complete;

    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.LogError("IAP_PURCHASE_FAILED:" + failureReason.ToString());
        OnItemPurchasedFailed?.Invoke(product);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        _storeController = controller;
        _storeExtentionProvider = extensions;
        Debug.Log("*********************");
        _storeExtentionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(result =>
        {
            if (result)
            {
                // This does not mean anything was restored,
                // merely that the restoration process succeeded.
            }
            else
            {
                // Restoration failed.
            }
        });
        isInitialized = true;
        OnInit?.Invoke(false);
        UIManager.GetLoadingUI().Hide();


    }

    public ProductMetadata GetProductMetadata(Product pr)
    {
        Debug.Log(pr.metadata.localizedPrice);
        return pr.metadata;
    }
    public void PurchaseProduct(string productID)
    {
        Debug.Log("Purchase INIT");
        if (_storeController.products.WithID(productID).availableToPurchase)
            _storeController.InitiatePurchase(productID);
        else
        {
            Debug.LogError(productID + " Not Available to Purchase");
        }
    }
    public Product GetProduct(string id)
    {
        Debug.Log("In Get" + _storeController.products.all[0].definition.id);
        return _storeController.products.WithID(id);
    }
    /// <summary>
    /// 1=purchased
    /// 0=not
    /// </summary>
    /// <param name="id"></param>
    /// <param name="val"></param>
    public void SaveIAP(string id, int val = 1)
    {
        PlayerPrefs.SetInt(id, 1);
    }
    public static bool CheckIfitemPurchased(string id)
    {
        var product = _storeController.products.all.First(x => x.definition.id == id);
        return product.hasReceipt;

    }
    public void LoadIAPLocally()
    {
        foreach (var item in _catalog.allProducts)
        {
            var itemState = PlayerPrefs.GetInt(item.id, 0);
            var arobject = ARManager.Instance._ARElements.Find(x => x._TargetID == item.id);
            if (itemState == 1)
            {
                arobject._isPurchased = true;

            }
            else
            {
                arobject._isPurchased = false;
            }
        }
        OnInit?.Invoke(true);
    }
    //public void PurchaseProduct(Product product)
    //{
    //    Debug.Log("Purchase INIT");

    //    _storeController.InitiatePurchase(product);
    //}



    /*  public void PurchaseItem(string ID)
      {
          PlayerPrefs.SetString(ID, "true");
          var arobject = ARManager.Instance._ArTargets.Find(x => x._TargetID == ID);
          arobject._ARTarget.SetActive(true);
          ARManager.Instance._ARElements.Find(y => y._ARObject == arobject._ARTarget)._isPurchased = true;
          AppDataManager.Instance.SavePrintablePagePurchse();
      }


      public void PurchaseFaild(Product pr, PurchaseFailureReason res)
      {
          Debug.LogError(res.ToString());
          switch (res)
          {
              case PurchaseFailureReason.PurchasingUnavailable:
                  break;
              case PurchaseFailureReason.ExistingPurchasePending:
                  break;
              case PurchaseFailureReason.ProductUnavailable:
                  break;
              case PurchaseFailureReason.SignatureInvalid:
                  break;
              case PurchaseFailureReason.UserCancelled:
                  break;
              case PurchaseFailureReason.PaymentDeclined:
                  break;
              case PurchaseFailureReason.DuplicateTransaction:
                  break;
              case PurchaseFailureReason.Unknown:
                  break;
              default:
                  break;
          }
      }

      public void OnInitializeFailed(InitializationFailureReason error)
      {
      }

      public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
      {
          return PurchaseProcessingResult.Complete;
      }

      public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
      {
      }

      public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
      {
          Debug.LogError("B");

          _storeController = controller;

          _storeExtentionProvider = extensions;
          Debug.Log("INITIALIZED......");
          foreach (var item in controller.products.all)
          {
              Debug.LogError(item.definition.id + "==========");
          }
      }*/
}
