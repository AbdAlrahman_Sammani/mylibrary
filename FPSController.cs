﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour
{
    

    // Update is called once per frame
    void Update()
    {
        if(Application.platform==RuntimePlatform.WindowsEditor)
        FpsController();

    }
    public void FpsController()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        transform.Translate(Vector3.forward * vertical * Time.deltaTime);
        transform.Rotate(Vector3.up * horizontal);
    }
}
