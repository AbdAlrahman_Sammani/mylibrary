﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// This Script Rotate The Camera With GyroScope 
/// Make The Camera a child Object to a parent and put this code in it
namespace AbdLibrary
{
    public class GyroScript : MonoBehaviour
    {
        private bool gyroEnabled;
        private Gyroscope gyro;
        private Quaternion rot;
        public GameObject CameraContainer;
        void Start()
        {
            gyroEnabled = EnableGyro();
        }

        // Update is called once per frame
        void Update()
        {
            if (gyroEnabled)
            {
                transform.localRotation = gyro.attitude * rot;
            }
        }

        private bool EnableGyro()
        {
            if (SystemInfo.supportsGyroscope)
            {
                gyro = Input.gyro;
                gyro.enabled = true;
                CameraContainer.transform.rotation = Quaternion.Euler(90, 90, 0);
                rot = new Quaternion(0, 0, 1, 0);

                return true;

            }
            else
            {
                return false;
            }
        }
    }
}