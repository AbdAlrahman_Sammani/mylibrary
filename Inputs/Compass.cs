﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbdLibrary
{
    public class Compass : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Input.location.Start();

            Debug.Log(Input.compass.magneticHeading);
            Debug.Log(Input.compass.trueHeading);

            transform.rotation = Quaternion.Euler(0, Input.compass.trueHeading, 0);
        }
        void OnGUI()
        {
            GUILayout.Label("Magnetometer reading: " + Input.compass.rawVector.ToString());
        }
    }
}
