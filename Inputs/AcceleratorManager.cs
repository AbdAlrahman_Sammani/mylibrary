﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcceleratorManager : MonoBehaviour
{
    public static Vector3 acceleratorInputValue;
    public static bool isDeviceHoldedFlat;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ReadAccelerator();
    }
    public void ReadAccelerator()
    {
        acceleratorInputValue = Input.acceleration.normalized;
        float yVlaue = acceleratorInputValue.y;
        if (Mathf.Abs(yVlaue) < 0.8)
        {
            isDeviceHoldedFlat = true;
        }
        else
        {
            isDeviceHoldedFlat = false;
        }

    }
 
}
