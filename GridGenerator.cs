﻿using System.Collections;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{
    [System.Serializable]
    public class PositionModel
    {
        public Vector2 position;
        public Vector2 reference;
        public bool state;
    }
    public List<PositionModel> positonsList = new List<PositionModel>();
    public static Dictionary<Vector2, Vector2> _2dGridReferences = new Dictionary<Vector2, Vector2>();
    public static Dictionary<Vector2, bool> _2dGridDictionary = new Dictionary<Vector2, bool>();

    public static Vector2[,] _grid2dMatix;

    public List<Vector2> gridPositionsList;
    public static List<float> xPositionsList = new List<float>();
    public static List<float> yPositionsList = new List<float>();

    public float spaceBetweenGrids = 1;
    public Vector2 startOffset, endOffset;
    public Vector3 screenSizeInworldSpace;

    public GameObject testObjectPrefab;
    public List<GameObject> testObjectsList;
    public bool testMode;
    public int unusedPositons;
    [SerializeField]
    // Start is called before the first frame update
    void Start()
    {
        Generate2dGrid();
        if (testMode)
        {
            InstantiateTestPrefab2();
        }
        Debug.Log("A===="+unusedPositons);
    }

    // Update is called once per frame
    void Update()
    {
        ToggleTestPrefabObject();
    }
    public Vector3 GetPlayAreaLimits_2d()
    {
        var screenwidth = Screen.width;
        var screenHeight = Screen.height;
        var screenMaxValue = Camera.main.ScreenToWorldPoint(new Vector3(screenwidth, screenHeight, 0));
        screenSizeInworldSpace = screenMaxValue;
        return screenMaxValue;
    }
    int xcount, ycount;
    public void Generate2dGrid()
    {
        var screenMaxValues = GetPlayAreaLimits_2d();
        for (float i = -screenMaxValues.x+startOffset.x; i <= screenMaxValues.x+endOffset.x; i += spaceBetweenGrids)
        {
            //  xPositionsList.Add(i);
            ycount = 0;
            for (float y = -screenMaxValues.y+startOffset.y; y <= screenMaxValues.y+endOffset.y; y += spaceBetweenGrids)
            {

                //   gridPositionsList.Add(new Vector2(i, y));
                //   _2dGridReferences.Add(new Vector2(xcount, ycount), new Vector2(i, y));
                PositionModel _position = new PositionModel();
                _position.position = new Vector2(i, y);
                _position.reference = new Vector2(ycount, xcount);
                _position.state = false;
                positonsList.Add(_position);
                ycount += 1;
                // yPositionsList.Add(y);
            }
            xcount += 1;
        }
        Debug.Log(xcount + "ss" + ycount);
        //    _grid2dMatix = new Vector2[xPositionsList.Count, yPositionsList.Count];
       /* _grid2dMatix = new Vector2[xcount, ycount];
        int counter = 0;
        for (int i = 0; i < xcount; i++)
        {
            for (int j = 0; j < ycount; j++)
            {
               // Debug.Log("[" + i + "," + j + "]");
                int val = (i + 1) * j;
              //  Debug.Log("val=" + counter);

                _grid2dMatix[i, j] = new Vector2(gridPositionsList[counter].x, gridPositionsList[counter].y);
                _2dGridDictionary.Add(new Vector2(i, j), false);
                counter++;

            }
        }*/
    }
    private void OnDrawGizmos()
    {
        //foreach (var item in positonsList)
        //{
        //    Gizmos.DrawCube(item.position, Vector3.one);

        //}
    }
    public void InstantiateTestPrefab()
    {
        foreach (var item in positonsList)
        {
            GameObject ob = Instantiate(testObjectPrefab,item.position, Quaternion.identity);

        }
        /*  for (int i = 0 + (int)startOffset.x; i < _grid2dMatix.GetLength(0) - endOffset.x; i++)
          {
              for (int j = 0 + (int)startOffset.y; j < _grid2dMatix.GetLength(1) - endOffset.y; j++)
              {
                  GameObject ob = Instantiate(testObjectPrefab, _grid2dMatix[i, j], Quaternion.identity);
                  testObjectsList.Add(ob);
                  if (j >= 4 && j <= 6)
                  {
                      if (i <= 1)
                      {
                          ob.GetComponent<MeshRenderer>().material.color = Color.red;
                      }
                      if (i >= xcount - 1)
                      {
                          ob.GetComponent<MeshRenderer>().material.color = Color.red;
                      }
                  }
                  else if (j < 4)
                  {
                      if (i > 1 && i < xcount - 1)
                      {
                          ob.GetComponent<MeshRenderer>().material.color = Color.red;
                      }

                  }

              }
          }*/
    }
    public int GetUnusedPositionsCount()
    {
        int counter = 0;
        for (int i = 0 + (int)startOffset.x; i < _grid2dMatix.GetLength(0) - endOffset.x; i++)
        {
            for (int j = 0 + (int)startOffset.y; j < _grid2dMatix.GetLength(1) - endOffset.y; j++)
            {
                if (j >= 0 && j <= 3)
                {
                    if (i > 0 && i < xcount - 1 && j < 3)
                    {
                        counter++;
                        _2dGridDictionary.Remove(new Vector2(i, j));
                    }
                    //if (i >= xcount - 1)
                    //{
                    //    ob.GetComponent<MeshRenderer>().material.color = Color.red;
                    //}
                }
                //else if (j < 4)
                //{
                //    if (i > 1 && i < xcount - 1)
                //    {
                //        ob.GetComponent<MeshRenderer>().material.color = Color.red;
                //    }

                //}

            }
        }


        return counter;

    }
    public void InstantiateTestPrefab2()
    {
        foreach (var item in positonsList)
        {
            GameObject ob = Instantiate(testObjectPrefab, item.position, Quaternion.identity);

        }
        //for (int i = 0 + (int)startOffset.x; i < _grid2dMatix.GetLength(0) - endOffset.x; i++)
        //{
        //    for (int j = 0 + (int)startOffset.y; j < _grid2dMatix.GetLength(1) - endOffset.y; j++)
        //    {
        //        GameObject ob = Instantiate(testObjectPrefab, _grid2dMatix[i, j], Quaternion.identity);
        //        testObjectsList.Add(ob);
        //        if (j >= 0 && j <= 3)
        //        {
        //            ob.GetComponent<MeshRenderer>().material.color = Color.red;

        //            if (i > 0 && i < xcount - 1 && j < 3)
        //            {
        //                ob.GetComponent<MeshRenderer>().material.color = Color.blue;
        //            }

        //        }
        //    }
        //}
    }
    public void ToggleTestPrefabObject()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            foreach (var item in testObjectsList)
            {
                item.GetComponent<MeshRenderer>().enabled = !item.GetComponent<MeshRenderer>().enabled;
            }
        }
    }
    public List<Vector2> GetRandomPosition()
    {

        try
        {
            var randomx = (int)Random.Range(startOffset.x, xcount - endOffset.x);
            var randomy = (int)Random.Range(startOffset.y, ycount - endOffset.y);
            if (randomy >= 4 && randomy <= 5)
            {
                if (randomx <= 1)
                {
                    return GetRandomPosition();
                }
                if (randomx >= xcount - 1)
                {
                    return GetRandomPosition();
                }
            }
            else if (randomy < 4)
            {
                if (randomx > 1 && randomx < xcount - 1)
                {
                    return GetRandomPosition();
                }
            }
            Vector2 rands = new Vector2(randomx, randomy);
            if (_2dGridDictionary[rands] == false)
            {
                var pos = _grid2dMatix[randomx, randomy];
                _2dGridDictionary[rands] = true;
                List<Vector2> result = new List<Vector2>();
                result.Add(pos);
                Vector2 relatedRef = new Vector2(randomx, randomy);
                result.Add(relatedRef);
                return result;
            }
            else
            {
                return GetRandomPosition();

            }
        }
        catch (System.StackOverflowException)
        {
            // Destroy(FindObjectOfType<EnemyBehaviour>().gameObject);
            // _2dGridDictionary.Clear();
            //for (int i = 0; i < _2dGridDictionary.Count; i++)
            //{
            //    var key = _2dGridDictionary.ElementAt(i);
            //    _2dGridDictionary[key.Key] = false;
            //    Debug.Log(_2dGridDictionary[key.Key]);
            //}
            return null;
        }

    }

    public Vector2 GetRandomPosition2()
    {
        int unusedCound = _2dGridDictionary.Count((x) => x.Value == false);
        Debug.Log("COU==" + unusedCound);
        if (unusedCound == unusedPositons)
        {
            ResetGridDictionary();
            unusedCound = 0;
            Debug.LogError("UNUSED===" + unusedCound);

            return GetRandomPosition2();
        }
        try
        {
            var randomx = (int)Random.Range(startOffset.x, xcount - endOffset.x);
            var randomy = (int)Random.Range(startOffset.y, ycount - endOffset.y);

            Vector2 rands = new Vector2(randomx, randomy);


            if (!_2dGridDictionary.ContainsKey(rands))
            {
                return GetRandomPosition2();
            }

            if (_2dGridDictionary[rands] == false)
            {
             
                var pos = _grid2dMatix[randomx, randomy];
                _2dGridDictionary[rands] = true;
                return pos;
            }
            else
            {
                return GetRandomPosition2();
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError("B");

            Debug.LogError(e.Message);
            ResetGridDictionary();
            return GetRandomPosition2();
        }

    }
    public void ClearPositionState(Vector2 val)
    {
        _2dGridDictionary[val] = false;
    }
    public void ResetGridDictionary()
    {
        for (int i = 0; i < _2dGridDictionary.Count; i++)
        {
            _2dGridDictionary[_2dGridDictionary.ElementAt(i).Key] = false;
        }
    }
    public void ResetAllGridObjectState()
    {
        positonsList.ForEach((x) => x.state = false);
    }
    public PositionModel GetRandomPosition3(List<Vector2> unusedPosition)
    {
        var li = from item in positonsList
                 where !unusedPosition.Contains(item.reference)
                 select item;
        var newlist = li.ToList();
        newlist = (from pos in newlist 
                  where pos.state==false
                  select pos).ToList() ;
        if (newlist.Count > 0) {
            int randomNum = Random.Range(0, newlist.Count);
            var model = newlist[randomNum];
            return model;
        }
        else
        {
            foreach (var item in positonsList)
            {
                SetPostionsState(item, false);
            }
            return GetRandomPosition3(unusedPosition);
        }
        
    }
    public void SetPostionsState(PositionModel model,bool state)
    {
        model.state = state;
    }
}
