﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
namespace AbdLibrary
{
    public class DrawOn2D : MonoBehaviour
    {
        public Texture2D tex;
        public Image drawingBoard;
        public Camera cam;
        // Vector2 pixelUV;
        //  Color[] co = new Color[25];
        public bool Erase, resetall;
        private Vector2 dragStart, dragEnd;
        public float brushSize;
        public float hardness;
        public Color drawingColor;
        public Color boardColor;

        public virtual void Start()
        {
            ResetAll();
        }

        public virtual void Update()
        {
            Draw2D();
        }


        public void Draw2D()
        {
            dragStart = dragEnd;
            Vector2 mouse = Input.mousePosition;
            tex = drawingBoard.sprite.texture;
            Debug.Log(tex.width);

            if (Input.GetMouseButtonDown(0))
            {
                if (EventSystem.current.IsPointerOverGameObject() && EventSystem.current.currentSelectedGameObject.GetComponent<DrawOn2D>())
                {
                    Debug.Log("StartDrawing");
                    dragStart = Input.mousePosition;
                }
            }
            if (Input.GetMouseButton(0))
            {
                if (dragStart == Vector2.zero)
                {
                    return;
                }
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    {
                        dragEnd = Input.mousePosition;
                        Drawing.PaintLine(dragStart, dragEnd, brushSize, drawingColor, hardness, tex);
                        tex.Apply();

                    }
                }
            }
        }
        public void ColorChanger(Color color, bool en)
        {
            Erase = en;

            if (en)
            {
                drawingColor = Color.white;
            }
            else
            {
                drawingColor = color;
            }
            /*     for (int i = 0; i < co.Length; i++)
                 {
                     if (en)
                     {
                         co[i] = Color.black;
                     }
                     else
                     {
                         co[i] = drawingColor;
                     }*/
        }
        public void ResetAll()
        {
            for (int i = 0; i < tex.width; i++)
            {
                for (int v = 0; v < tex.height; v++)
                {
                    tex.SetPixel(i, v, Color.black);
                }
            }

            tex.Apply();
        }
    }
}


