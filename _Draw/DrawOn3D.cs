﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace AbdLibrary
{
    public class DrawOn3D : MonoBehaviour
    {
        public Texture2D tex;
        //   public  Vector2 lowerPosition;
        //  public Vector2 higherPosition;

        RaycastHit hit;
        public Camera cam;
        //  Renderer rend;
        //    Vector2 pixelUV;

        public GameObject drawingBoard;
        public bool Erase, resetall;
        //   public List<Vector2> allpixles = new List<Vector2>();
        //  public Vector3 startPoint, endPoint;
        //  int first_Px, second_px, first_py, second_py;
        private Vector2 dragStart, dragEnd;
        public float brushSize;
        public float hardness;
        public Color drawingColor;

        public Color boardColor;
        private Color[] resetColors;
        private bool isOut;
        //  private float startPos_X, startPos_Y, endPos_X, endPos_Y, ques_Start_X, ques_Start_Y, ques_End_X, ques_end_y, ques_Key_StartX, ques_Key_StartY, ques_Key_EndX, ques_Key_EndY;
        // Use this for initialization
        public virtual void Start()
        {
            resetColors = new Color[tex.width * tex.height];
            tex = drawingBoard.GetComponent<MeshRenderer>().material.mainTexture as Texture2D;
            SetResetColor(boardColor);
        }

        // Update is called once per frame
        public virtual void Update()
        {

        }

        public void SetResetColor(Color color)
        {
            for (int i = 0; i < resetColors.Length; i++)
            {
                resetColors[i] = color;
            }
        }
     
  
        public void Draw()
        {

            if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit))
            {
                isOut = true;
                return;
            }

            dragStart = dragEnd;

            Vector2 mouse = Input.mousePosition;

            //    rend = hit.transform.GetComponent<Renderer>();


            //  pixelUV = hit.textureCoord;

            if (Input.GetMouseButtonDown(0))
            {
                dragStart = hit.textureCoord;
                dragStart.x *= tex.width;
                dragStart.y *= tex.height;
            }
            if (Input.GetMouseButton(0))
            {
                if (dragStart == Vector2.zero)
                {
                    return;
                }
                dragEnd = hit.textureCoord;
                dragEnd.x *= tex.width;
                dragEnd.y *= tex.height;
                if (isOut == true) dragStart = dragEnd;


                Drawing.PaintLine(dragStart, dragEnd, brushSize, drawingColor, hardness, tex);

                tex.Apply();
                isOut = false;

            }
        }


        public void Erase_ColorChanger(Color color, bool en) // en means eraser enabled or not 
        {
            Erase = en;

            if (en)
            {

                drawingColor = boardColor;

            }
            else
            {
                drawingColor = color;
            }
        }
        public void ResetAll()
        {
            SetResetColor(boardColor);
            tex.SetPixels(resetColors);
           

            tex.Apply();
        }

        public void ToggleEraser(bool state)
        {
            Erase = state;
        }
    }
}

  



