using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SearchedItemAttributes : MonoBehaviour
{
    public string itemName;
    public string[] ItemKeywords; //{ get {// return LanguageManager.Instance.GetRawStringArray(string.Format("Places/{0}/Keywords", itemName)); } }
    public string itemID;
    public GameObject mapReference;
    public int floorNumber;

    private Text textElement;

    private void Start()
    {
        textElement = GetComponentInChildren<Text>();

      //  if (textElement)
          //  textElement.text = LanguageManager.Instance.GetString(string.Format("Places/{0}/Name", itemName));
    }
}
