using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SearchSystem : MonoBehaviour
{
    public GameObject mainPanel;
    public GameObject resultPanel;
    public InputField searchInput;
    public Behaviour noResult;
    public List<string> wordsList = new List<string>();
    public List<GameObject> mainPlaces = new List<GameObject>();
    public List<GameObject> inSearchMenuplaces = new List<GameObject>();
    IEnumerable<GameObject> queryForPlace;
    private bool resultState;
    // Use this for initialization
    private void Update()
    {
        
    }
    public void ShowSearchPanel(bool val)
    {
        mainPanel.GetComponentInParent<Animator>().SetBool("searchResult", val);
        if (val == false)
        {
            foreach (var place in inSearchMenuplaces)
            {
                place.SetActive(false);
            }
        }
    }
    public void SearchForPlaces()
    {
        foreach (var place in inSearchMenuplaces)
        {
            place.SetActive(false);
        }
        string wordToSearch = searchInput.text;
        if (wordToSearch == "")
        {
            noResult.enabled = true;
            foreach (var item in inSearchMenuplaces)
            {
                item.SetActive(false);
                //selected = null;
            }
            return;
        }
        else
        {
            foreach (var item in inSearchMenuplaces)
            {
                queryForPlace = from attr in item.GetComponent<SearchedItemAttributes>().ItemKeywords
                                where attr.ToLower().Contains(wordToSearch.ToLower()) && wordToSearch.Count<char>() > 1
                                select item.gameObject;
                if (queryForPlace.Count<GameObject>() !=0)
                {
                    resultState = true;
                }
                if (resultState)
                {
                    noResult.enabled = false;
                    foreach (GameObject go in queryForPlace)
                    {
                        Debug.Log(go.name);
                        go.SetActive(true);
                    }
                }
                else
                {
                    foreach (GameObject go in inSearchMenuplaces)
                    {
                        go.SetActive(false);
                        noResult.enabled = true;
                    }
                }
            }
            ShowSearchPanel(true);
            resultState = false;
        }
    }
    public void FilterResult(string name)
    {
        searchInput.text = name;
        SearchForPlaces();
        searchInput.text = "";
    }
}
