//using UnityEngine;
//using System.Collections;
//using UnityEngine.UI;
//using System.Text;

//[RequireComponent(typeof(Text))]
//[ExecuteInEditMode]
//public class RtlText1 : MonoBehaviour
//{
//    public string text;
//    public bool multiline = true;

//    string originalString;
//    Text label;
//    string visualString;

//    void Awake()
//    {
//        label = GetComponent<Text>();
//        visualString = NBidi.NBidi.LogicalToVisual(text ?? "");
//        originalString = text;
//    }

//    void Update()
//    {
//        var leftComp = string.IsNullOrEmpty(text) ? "" : text;
//        var rightComp = string.IsNullOrEmpty(originalString) ? "" : originalString;
//        if (!leftComp.Equals(rightComp))
//            UpdateLabel();
//    }

//    public void UpdateLabel()
//    {
//        visualString = NBidi.NBidi.LogicalToVisual(text ?? "");
//        originalString = text;
//        label.text = visualString;

//        if (!multiline)
//            return;

//        try
//        {
//            label.cachedTextGenerator.Populate(visualString, label.GetGenerationSettings(GetComponent<RectTransform>().sizeDelta));
//            var lines = label.cachedTextGenerator.GetLinesArray();

//            int takenCharCount = 0;

//            StringBuilder sb = new StringBuilder();

//            for (int i = lines.Length - 1; i >= 0; i--)
//            {
//                var lineStart = lines[i].startCharIdx;

//                var endIndex = visualString.Length - takenCharCount;
//                var lineLength = endIndex - lineStart;
//                sb.AppendLine(visualString.Substring(lineStart, lineLength).Trim());
//                takenCharCount += lineLength;
//            }

//            label.text = sb.ToString().Trim();
//        }
//        catch (System.Exception e)
//        {
//            Debug.Log(e);
//        }
//    }
//}