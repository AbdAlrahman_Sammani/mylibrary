﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace AbdLibrary
{
    public class On_Off_ObjectHandler : MonoBehaviour
    {
        public UnityEvent onDisableEvent, onEnableEvent;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void OnDisable()
        {
            onDisableEvent.Invoke();
        }
        public void OnEnabled()
        {
            onEnableEvent.Invoke();
        }
    }
}
