﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace AbdLibrary
{
    public enum Mode { Toggle, Button }
    public class Button3D : MonoBehaviour
    {
        public Color mouseOverColor;
        public Color ClickedColor;
        private Color primaryColor;
        public Mode mode;
        public UnityEvent onClick;

        // Use this for initialization
        void Start()
        {
            primaryColor = GetComponent<MeshRenderer>().material.color;

        }

        // Update is called once per frame
        void Update()
        {

        }
        private void OnMouseDown()
        {
            onClick.Invoke();
            GetComponent<MeshRenderer>().material.color = ClickedColor;
        }
        private void OnMouseOver()
        {
        }
        private void OnMouseExit()
        {
            GetComponent<MeshRenderer>().material.color = primaryColor;

        }
        private void OnMouseUp()
        {
            GetComponent<MeshRenderer>().material.color = primaryColor;

        }

    }
}
