﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace AbdLibrary
{
    public enum Button_Type { OneClick, AlwaysAcitve }
    public class ButtonsClicksManager : MonoBehaviour
    {
        public Button_Type buttonType;
        private Button button;
        public bool DoingActionAfterTime;
        public float timeToDoAction;

        public float disableTime;

        public UnityEvent callMethodAfterFinishedAction;
        public UnityEvent callMethodAfterTimeAction;
        public bool isWaitedActionFinished;
        public GameObject disInput;
        public string nameOfDisableInputPanel;
        private void Awake()
        {
            //disInput = GameObject.FindGameObjectWithTag("dis");
        }
        // Use this for initialization
        void Start()
        {
                
            disInput.SetActive(false);
            button = GetComponent<Button>();
            if (buttonType == Button_Type.OneClick)
                button.onClick.AddListener(() => HandleButton(disableTime));
             
        }
        private void Update()
        {
            
        }

        public IEnumerator DisableButtonForTime(float time)
        {
            button.interactable = false;
            disInput.SetActive(true);
            yield return new WaitForSeconds(time);
            button.interactable = true;
            isWaitedActionFinished = true;
            disInput.SetActive(false);
        }
        public void EnableButton()
        {
            button.interactable = false;
           // yield return new WaitForSeconds(time);
            button.interactable = true;
            disInput.SetActive(false);

        }
        public void HandleButton(float time)
        {
            button.interactable = false;
            disInput.SetActive(true);
            Invoke("EnableButton", time);
           // StartCoroutine(DisableButtonForTime(time));
            if (DoingActionAfterTime)
            {
                Invoke("DoActionAfterTime", timeToDoAction);
            }
        }


        public void DoActionWhenOtherFinished()
        {
            
            StartCoroutine(ButtonDoAction());
        }

        public void TestIsActionFinished()
        {
            isWaitedActionFinished = true;
        }
        public void DoActionAfterTime()
        {
            callMethodAfterTimeAction.Invoke();
        }
        public IEnumerator ButtonDoAction()
        {
           
            yield return new WaitUntil(() => isWaitedActionFinished == true);
            
            Debug.Log("Invoked");
            callMethodAfterFinishedAction.Invoke();
            isWaitedActionFinished = false;
        }
    }
}
