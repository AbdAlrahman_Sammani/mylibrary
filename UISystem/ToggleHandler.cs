﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;
namespace AbdLibrary
{
    public class ToggleHandler : MonoBehaviour,IPointerClickHandler
    {
        public List<Toggle> otherTogglesToTurnOff = new List<Toggle>();
        public UnityEvent toggleOnEvent;
        public UnityEvent toggleOffEvent;
        public Toggle toggle;
        public Button btn;
       // public EventTrigger eventTrigger;
        public bool useEventTrigger;
      
        void Start()
        {
            toggle = GetComponent<Toggle>();
            if (!useEventTrigger)
            {
                toggle.onValueChanged.AddListener((val) => ToggleON(true));
                toggle.onValueChanged.AddListener((val) => ToggleOff(true));
            }
          

        }
       
        public void ToggleON(bool val)
        {
            if (toggle.isOn)
            {
                if (otherTogglesToTurnOff.Count > 0) ToggleOther();
                toggleOnEvent.Invoke();
            }
        }
        public void ToggleOff(bool val)
        {
            if (!toggle.isOn)
            {
                toggleOffEvent.Invoke();
            }
        }
        public void OnPointerTrigger()
        {
            if (useEventTrigger)
            {
                if (toggle.isOn)
                {
                    if (otherTogglesToTurnOff.Count > 0) ToggleOther();
                    toggleOnEvent.Invoke();
                }
                else
                {
                    toggleOffEvent.Invoke();

                }
            }
        }
        public void ToggleOther()
        {
            foreach (var item in otherTogglesToTurnOff)
            {
                item.isOn = false;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (useEventTrigger)
            {

                OnPointerTrigger();
            }
        }
    }
}
