﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace AbdLibrary {
    public class CallEvent : MonoBehaviour
    {
        // Use this for initialization
        public UnityEvent MainEventToCall;
        public UnityEvent[] CertainEventToCall;
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void CallEventMethod()
        {
            MainEventToCall.Invoke();
        }
        public void CallCertainMethod(int id)
        {
            CertainEventToCall[id].Invoke();
        }
    }
}
