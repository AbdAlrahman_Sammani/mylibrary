﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public class TCPServer : MonoBehaviour
{
    #region private members 	
    /// <summary> 	
    /// TCPListener to listen for incomming TCP connection 	
    /// requests. 	
    /// </summary> 	
    private TcpListener tcpListener;
    /// <summary> 
    /// Background thread for TcpServer workload. 	
    /// </summary> 	
    private Thread tcpListenerThread;
    /// <summary> 	
    /// Create handle to connected tcp client. 	
    /// </summary> 	
    private TcpClient connectedTcpClient;
    #endregion

    // Use this for initialization
    private void Awake()
    {
       // Thread.CurrentThread.Abort();
    }
    void Start()
    {
        // Start TcpServer background thread 		
     //   Thread.CurrentThread.Abort();
        tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
       tcpListenerThread.IsBackground = true;
        tcpListenerThread.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            tcpListenerThread.Abort();
        }
    }

    /// <summary> 	
    /// Runs in background TcpServerThread; Handles incomming TcpClient requests 	
    /// </summary> 	
    private void ListenForIncommingRequests()
    {
        TcpListener server = null;
        try
        {
            // Set the TcpListener on port 13000.
            Int32 port = 13000;
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");

            // TcpListener server = new TcpListener(port);
            server = new TcpListener(localAddr, port);

            // Start listening for client requests.
            server.Start();

            // Buffer for reading data
            byte[] bytes = new Byte[512];
            String data = null;

            // Enter the listening loop.
            while (true)
            {
                Debug.Log("Waiting for a connection... ");

                // Perform a blocking call to accept requests.
                // You could also user server.AcceptSocket() here.
                TcpClient client = server.AcceptTcpClient();
                Debug.Log("Connected!");

                data = null;

                // Get a stream object for reading and writing
                NetworkStream stream = client.GetStream();

                int i;
                string result = "";
                // Loop to receive all the data sent by the client.
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    // Translate data bytes to a ASCII string.
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                    Debug.Log("Received: {0}" + data);
                    result = result + data;
                    // Process the data sent by the client.
                    data = data.ToUpper();

                    byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

                    // Send back a response.
                    stream.Write(msg, 0, msg.Length);
                    Debug.Log("Sent: {0}" + data);
                }
                Debug.Log("Result=" + result);
                // Shutdown and end connection
                client.Close();
            }
        }
        catch (SocketException e)
        {
            Debug.Log("SocketException: {0}" + e);
            server.Stop();
        }
        finally
        {
            // Stop listening for new clients.
            server.Stop();
            Debug.Log("Stopped");
           // ListenForIncommingRequests();

        }

        //	Debug.Log("\nHit enter to continue...");
        //	Console.Read();

    }
    private void OnApplicationQuit()
    {
        tcpListenerThread.Abort();
    }
   
}
/// <summary> 	
/// Send message to client using socket connection. 	
/// </summary> 	
//private void SendMessage()
//{
//	if (connectedTcpClient == null)
//	{
//		return;
//	}

//	try
//	{
//		// Get a stream object for writing. 			
//		NetworkStream stream = connectedTcpClient.GetStream();
//		if (stream.CanWrite)
//		{
//			string serverMessage = "This is a message from your server.";
//			// Convert string message to byte array.                 
//			byte[] serverMessageAsByteArray = Encoding.ASCII.GetBytes(serverMessage);
//			// Write byte array to socketConnection stream.               
//			stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);
//			Debug.Log("Server sent his message - should be received by client");
//		}
//	}
//	catch (SocketException socketException)
//	{
//		Debug.Log("Socket exception: " + socketException);


//	}
//}
