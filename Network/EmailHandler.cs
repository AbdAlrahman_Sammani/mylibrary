﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

public class EmailHandler : MonoBehaviour
{
    [System.Serializable]
    public struct MailParameters
    {
        public string emailAddress;
        public string emailPassword;
        public string emailSMTP;
        public string emailSubject;
        public string emailBody;
        public string emailReciver;
    }
    public MailParameters mailParameters;
    public Text emailInpuText;
    public GameObject errorMailMessage;
    public Button sendEmailButton;
    void Awake()
    {

        // DontDestroyOnLoad(transform.gameObject);
    }
    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
         mailParameters.emailReciver = emailInpuText.text;
        if (Input.GetKeyDown(KeyCode.A))
        {
            SendEmail();
        }
        checkMailValue();
    }
    public void ShowText()
    {
    }

    public void SendEmail()
    {
        if (checkMailValue())
        {
           // StartCoroutine(EmptyMailHandler(true));
           // return;

        }
        MailMessage mail = new MailMessage();

        mail.From = new MailAddress(mailParameters.emailAddress);
        mail.To.Add(mailParameters.emailReciver);
        mail.Subject = mailParameters.emailSubject;
        mail.Body = mailParameters.emailBody;
      //  Attachment at = new Attachment(Application.streamingAssetsPath + "/CameraPic/" + "CameraPic" + IM_Save.imagename + ".png");
      //  mail.Attachments.Add(at);

        SmtpClient smtpServer = new SmtpClient(mailParameters.emailSMTP);
        smtpServer.Port = 587;
        smtpServer.Credentials = new System.Net.NetworkCredential(mailParameters.emailAddress,mailParameters.emailPassword) as ICredentialsByHost;
        smtpServer.EnableSsl = true;
        if (true)
        {
            Debug.Log("UsingSSlCode");
            ServicePointManager.ServerCertificateValidationCallback =
                  delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                  { return true; };
        }
       
        string a = "";

        smtpServer.SendCompleted += (s, eventArgs) =>
        {
            if (eventArgs == null)
                Debug.LogError("Event Args is NULL!");
            else if (eventArgs.Error != null)
            {
                Debug.Log("Error happened when sending email!\n");
                Debug.LogError("The Reason is " + eventArgs.Error.Message);
                Debug.Log(eventArgs.Error);
                Debug.Log(eventArgs.Error.HelpLink);
                Debug.Log(eventArgs.Error.StackTrace);
            }
            else if (eventArgs.Cancelled)
                Debug.Log("Something canceled sending email!\n");
        };
        try
        {
            smtpServer.SendAsync(mail, a);
            Debug.Log("success");
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
        // ExitManager.appState = ExitManager.AppState.Last;

        ///update all ();the pic in Draw
        // RawImageUpdater.reReadTextures = true;

    }
    public bool checkMailValue()
    {
        if (IsValidEmail(mailParameters.emailReciver))
        {
            sendEmailButton.interactable = true;
            return true;

        }
        else
        {
            sendEmailButton.interactable = false;
            return false;
        }
    }
    public IEnumerator EmptyMailHandler(bool isempty)
    {
        errorMailMessage.GetComponent<Animator>().SetTrigger("On");
        errorMailMessage.SetActive(true);
        yield return new WaitForSeconds(3);
        //errorMailMessage.GetComponent<Animator>().SetTrigger("Off");
        //  yield return new WaitForSeconds(.6f);
        //   errorMailMessage.SetActive(false);


        //Show Image Say Draw Somthing for 3 sec

    }
    public void Delete_File(string info)
    {
        if (System.IO.File.Exists(info))
        {
            // Use a try block to catch IOExceptions, to
            // handle the case of the file already being
            // opened by another process.
            try
            {
                System.IO.File.Delete(info);
                Debug.Log("Deleted");
            }
            catch (System.IO.IOException e)
            {
                Debug.Log(e.Message);
                return;
            }
        }
        else
        {
            Debug.Log("NotFound");
        }
    }

    public IEnumerator Delete_After_Time(string inf)
    {
        yield return new WaitForSeconds(5);
        Delete_File(inf);
    }
    bool IsValidEmail(string strIn)
    {
        // Return true if strIn is in valid e-mail format.
        return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
    }
    
}
