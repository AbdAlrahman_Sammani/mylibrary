﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UDPServer : MonoBehaviour
{
	public static String UDPMessage;
	public bool multiPackets;
	public string allMessage;
	// read Thread
	Thread readThread;

	// udpclient object
	UdpClient client;

	// port number
	public int port = 5005;

	public static Action<string> OnMessageRecived;
	// start from unity3d
	void Start()
	{
		// create thread for reading UDP messages
		readThread = new Thread(new ThreadStart(ReceiveData));
		readThread.IsBackground = true;
		readThread.Start();
	}

	// Unity Update Function
	void Update()
	{
		// check button "s" to abort the read-thread
		if (Input.GetKeyDown("q"))
			StopThread();
	}

	// Unity Application Quit Function
	void OnApplicationQuit()
	{
		StopThread();
	}

	// Stop reading UDP messages
	private void StopThread()
	{
		if (readThread.IsAlive)
		{
			readThread.Abort();
		}
		client.Close();
	}

	// receive thread function
	private void ReceiveData()
	{
		client = new UdpClient(port);
		while (true)
		{
			try
			{
				// receive bytes
				IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, port);

				byte[] data = client.Receive(ref anyIP); 
				//Debug.Log("Data=" + data.Length);
				//string test = Encoding.ASCII.GetString(data);
				//testMes = test;
				if (data.Length > 0)
				{

					// encode UTF8-coded bytes to text format
				//	string text = Convert.ToBase64String(data);
				//	var d = Convert.FromBase64String(text);
					string	text = Encoding.ASCII.GetString(data);
					// show received message
					UDPMessage = text;
				//	Debug.Log("Message :" + UDPMessage);
					if (multiPackets)
					{
						if (UDPMessage.Contains("Start"))
						{
							allMessage = "";
						}
						allMessage += UDPMessage;
						if (UDPMessage.Contains("Finish"))
						{
						//	Debug.Log("Finish" + allMessage);
							OnMessageRecived.Invoke(allMessage);
						}
					}
					else
					{
						OnMessageRecived.Invoke(UDPMessage);
					}

				}

			}
			catch (Exception err)
			{
				print(err.ToString());
			}

		}
	}


	// return the latest message
	/*public string getLatestPacket()
	{
		allReceivedPackets = "";
		return lastReceivedPacket;
	}*/
}