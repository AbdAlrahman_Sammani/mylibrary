﻿using UnityEngine;
using System.Collections;
using System;
using System.Net.Sockets;
using System.Net;
using System.IO;

public class ArtNet : MonoBehaviour
{

    [SerializeField]
    private string _destinationIP = "127.0.0.1";

    [SerializeField]
    private byte _net = 0x1;
    [SerializeField]
    private byte _universe = 0x0;

    [SerializeField]
    private float _DMX_fps = 30;

    [SerializeField]
    [Range(0, 255)]
    private byte[] _data = new byte[512];


    private UdpClient _socket;
    private IPEndPoint _target;

    private byte[] _artNetPacket = new byte[530];

    private float _lastTxTime = 0;
    private float _intervalTime;

    public void Start()
    {
        
    }
    private void OnEnable()
    {
        UDPSocketSetup();
        ArtnetSetup();
        tx();
    }
    private void OnDisable()
    {
        _socket.Dispose();

    }
    private void UDPSocketSetup()
    {
        _target = new IPEndPoint(IPAddress.Parse(_destinationIP), 6454);

        _socket = new UdpClient();
        _socket.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        _socket.Connect(_target);
    }

    private void ArtnetSetup()
    {
        string str = "Art-Net";
        System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
        encoding.GetBytes(str, 0, str.Length, _artNetPacket, 0);

        _artNetPacket[7] = 0x0;

        //opcode low byte first
        _artNetPacket[8] = 0x00;
        _artNetPacket[9] = 0x50;

        //proto ver high byte first
        _artNetPacket[10] = 0x0;
        _artNetPacket[11] = 14;

        //TODO: Full Addressing

        //sequence 
        _artNetPacket[12] = 0x0;

        //physical port
        _artNetPacket[13] = 0x0;

        //universe low byte first
        _artNetPacket[14] = _universe;
        _artNetPacket[15] = _net;

        //length high byte first
        _artNetPacket[16] = ((512 >> 8) & 0xFF);
        _artNetPacket[17] = (512 & 0xFF);
    }

    private void tx()
    {
        Buffer.BlockCopy(_data, 0, _artNetPacket, 18, 512);

        try
        {
            _socket.Send(_artNetPacket, _artNetPacket.Length);
        }
        catch (Exception e)
        {
            Debug.Log(this + " " + e);
        }
    }

    public void Update()
    {
        //if (Input.GetKeyDown(KeyCode.H))
        Sentdata();

    }
    public void Sentdata()
    {
        //if (_data[6] > 200)
        //{
        //    _data[6] = 0;
        //}
        //else
        //{
        //    _data[6] += 1;
        //}
        _intervalTime = 1f / _DMX_fps;

        if (Time.time - _lastTxTime >= _intervalTime)
        {
            _lastTxTime = Time.time;
            tx();
        }
    }
    private void OnApplicationQuit()
    {
        if(_socket!=null)
        _socket.Dispose();

    }
    public  void UpdateDataBytes(int id,int value)
    {
        _data[id] = byte.Parse(value.ToString()) ;
    }
    public void ModifyParameters(string address,int net,int universe)
    {
        _destinationIP = address;
        _net = byte.Parse(net.ToString());
        _universe = byte.Parse(universe.ToString()) ;
    }
    public void ToggleState(bool start_stop)
    {
        enabled = start_stop;
    }
}