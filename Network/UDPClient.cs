﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class UDPClient : MonoBehaviour
{
    // Start is called before the first frame update
    private static string prefix = "Start:";
    private static string suffix = ":Finish";
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void SendUDPMessage(string message, bool multiPackets, int port)
    {
        //   while (true)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);

            Debug.Log(data.Length + "=Length");
            //   string mes = Encoding.ASCII.GetString(data);


            Debug.Log("Lenght=" + data.Length);
            string ipAddress = "127.0.0.1";
            int sendPort = port;


            try
            {
                using (var client = new UdpClient())
                {
                    IPEndPoint ep = new IPEndPoint(IPAddress.Parse(ipAddress), sendPort);
                    client.Connect(ep);
                    if (multiPackets)
                    {
                        string messageTosend = prefix + data.Length + ":" + message + suffix;
                        byte[] rawdatatoSend = Encoding.ASCII.GetBytes(messageTosend);
                        byte[] datatosend = new byte[1024 * 60];
                        int remainingData = rawdatatoSend.Length;
                        int startIndex = 0;
                        int remainCounter = 0;

                        while (remainingData > 0)
                        {
                            for (int i = 0; i < datatosend.Length; i++)
                            {
                                if (i + startIndex > rawdatatoSend.Length - 1)
                                {
                                    // Console.WriteLine("END=" + (i + startIndex));
                                    datatosend[i] = 0;
                                    // break;
                                }
                                else
                                {
                                    datatosend[i] = rawdatatoSend[(i + startIndex)];
                                    remainCounter++;
                                }
                            }
                            remainingData -= remainCounter;
                            startIndex += datatosend.Length;
                            client.Send(datatosend, datatosend.Length);
                            string mes = Encoding.ASCII.GetString(datatosend);
                            remainCounter = 0;
                            Debug.Log("StartIndex=" + startIndex);
                            Debug.Log("Remaining=" + remainingData);
                            Debug.Log("Message:" + mes);
                            Debug.Log("A=" + datatosend.Length);

                        }
                    }
                    else
                    {
                        // while (data.Length > 0)
                        //    {
                        string messageTosend = prefix + message + suffix;
                        byte[] rawdatatoSend = Encoding.ASCII.GetBytes(messageTosend);
                        client.Send(rawdatatoSend, rawdatatoSend.Length);
                        //  }
                    }

                }

            }
            catch (Exception ex)
            {
                Debug.Log("ex:" + ex.ToString());
            }

        }
    }
}
