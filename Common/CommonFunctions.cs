﻿using System;
using System.Diagnostics;
using System.IO;
using UnityEngine;
public class CommonFunctions : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public Sprite LoadImageFromStreamingAssets(string path)
    {
        byte[] imageBytes = File.ReadAllBytes(path);
        Texture2D tex = new Texture2D(800, 800);
        tex.LoadImage(imageBytes);
        Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, .5f), 100);
        return sprite;
    }
    public static void Open_EXE_File(string path)
    {
        Process myProcess = new Process();

        try
        {
            myProcess.StartInfo.UseShellExecute = false;
            // You can start any process, HelloWorld is a do-nothing example.
            myProcess.StartInfo.FileName = path;
            myProcess.StartInfo.CreateNoWindow = true;
            myProcess.Start();
            // This code assumes the process you are starting will terminate itself.
            // Given that is is started without a window so you cannot terminate it
            // on the desktop, it must terminate itself or you can do it programmatically
            // from this application using the Kill method.
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

    }
    public static void CloseEXEApplication(string name)
    {
        Process[] processes = Process.GetProcessesByName(name);
        foreach (var process in processes)
        {
            process.Kill();
        }
    }
}
