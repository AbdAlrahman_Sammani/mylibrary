﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
public class Timer : MonoBehaviour
{
   public enum Type { Seconds,Full}
    public Type _type;
    // Start is called before the first frame update
    public float gameTime;
    public float remainingTime;
    public int minutes;
    public int hours;
    public bool useTimer;
    public bool useUI;
    public Text[] timeText;
    public UnityEvent onTimerEnd;
    public UnityEvent on10seconds;

    bool second10EventInvoked;

    public int uiHour, uimin, uirem;
    void Start()
    {
        remainingTime = gameTime;
        minutes = Mathf.CeilToInt(remainingTime / 60);
        hours = Mathf.CeilToInt(minutes / 60);
        if (useUI)
            UpdateTimerUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (useTimer)
        {
            TimerProcess();
        }
    }
    public void TimerProcess()
    {
        if (remainingTime >= 0)
        {
            remainingTime -= Time.deltaTime;
            minutes = Mathf.FloorToInt(remainingTime / 60);
            hours = Mathf.FloorToInt(minutes / 60);
            if (useUI)
                UpdateTimerUI();
        }
        else
        {
            remainingTime = gameTime;
            onTimerEnd.Invoke();
            useTimer = false;
            //            EndGame();
        }
        if (Mathf.FloorToInt(remainingTime) == 10 && second10EventInvoked == false)
        {
            on10seconds.Invoke();
            second10EventInvoked = true;
        }
        
    }
    public void StartTimer()
    {
        useTimer = true;
        second10EventInvoked = false;
    }
    public void StopTimer()
    {
        useTimer = false;
        remainingTime = gameTime;

    }
    public void UpdateTimerUI()
    {
        if (_type == Type.Full)
        {
            int rem = (int)remainingTime;
            int min = (int)minutes;
            uirem = rem;
            uimin = min;
            uiHour = hours;
            if (rem >= 60)
            {
                int mult = 60 * (minutes);
                rem = rem - mult;
                uirem = rem;
            }
            if (min >= 60)
            {
                int mult = 60 * (hours);
                min = min - mult;
                uimin = min;
            }
            if (minutes >= 0)
            {
                foreach (var item in timeText)
                {
                    //  item.text =(hours).ToString("00") +":"+ ((minutes - 1)/60).ToString("00") + ":" + Mathf.FloorToInt(rem).ToString("00");
                    item.text = (hours).ToString("00") + ":" + Mathf.FloorToInt(min).ToString("00") + ":" + Mathf.FloorToInt(rem).ToString("00");
                    uirem = Mathf.FloorToInt(rem);
                    uimin = Mathf.FloorToInt(min);
                    uiHour = hours;
                    // value = new Vector3(hours, min, rem);
                }
            }
        }
        if (_type == Type.Seconds)
        {
            foreach (var item in timeText)
            {
                int rem = (int)remainingTime;
                item.text = Mathf.FloorToInt(rem).ToString("00");
            }
        }

        //   minutesText.text = "Remaining=" + rem.ToString();

    }
    public void ResetTimer()
    {
        StopTimer();
        remainingTime = gameTime;
        minutes = Mathf.CeilToInt(remainingTime / 60);
        if (useUI)
            UpdateTimerUI();
    }

    public void SetTimerValues(Vector2 newTime)
    {
        gameTime = newTime.y*60;
        //minutes = (int)newTime.y*60;
        ResetTimer();
    }
  
}
