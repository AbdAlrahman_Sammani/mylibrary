﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
public class Clock : MonoBehaviour
{
    public TextMeshProUGUI clockText;
    public TextMeshProUGUI dateText;
    // Start is called before the first frame update
    void Start()
    {
        GetCurrentTime();
    }

    // Update is called once per frame
    void Update()
    {
        GetCurrentTime();
    }
    public void GetCurrentTime()
    {
        clockText.text = DateTime.Now.Hour + ":" + DateTime.Now.Minute.ToString("D2");
        dateText.text = DateTime.Now.Year + " " + DateTime.Now.Month + " " + DateTime.Now.Day + " " + DateTime.Now.DayOfWeek;
        Debug.Log(DateTime.Now.Hour + ":" + DateTime.Now.Minute);
        Debug.Log(DateTime.Now.Date.ToShortDateString());
    }
}
