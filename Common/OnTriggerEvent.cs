﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class OnTriggerEvent : MonoBehaviour
{
    public UnityEvent onTriggerEnter, onTriggerExit, onTriggerStay,onColliderEnter;
    public string otherTag;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag(otherTag))
        {
            onColliderEnter.Invoke();
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag(otherTag))
        {
            onTriggerEnter.Invoke();

        }
    }
    private void OnTriggerExit(Collider other)
    {

        if (other.CompareTag(otherTag))
        {
            onTriggerExit.Invoke();


        }
    }
    private void OnTriggerStay(Collider other)
    {

        if (other.CompareTag(otherTag))
        {
            onTriggerStay.Invoke();

        }
    }

    
}
