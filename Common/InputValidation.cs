﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Net;
namespace AbdLibrary
{
    public class InputValidation : MonoBehaviour
    {
        public enum InputType { Email,IP,Name,Number}
        public InputType _inputType;
        public  InputField _inputField;

        // Start is called before the first frame update
        void Start()
        {
            _inputField = GetComponent<InputField>();
        }

        // Update is called once per frame
        void Update()
        {
            switch (_inputType)
            {
                case InputType.Email:
                    break;
                case InputType.IP:
                    ValidateIp();
                    break;
                case InputType.Name:
                    break;
                case InputType.Number:
                    break;
                default:
                    break;
            }
        }
        public  bool ValidateIp()
        {
            string _text = _inputField.text;
            if (_text.Any(char.IsLetter))
            {
                _inputField.text = _inputField.text.Remove(_inputField.text.Length - 1, 1);
                return false;
            }

            if (_text.Count(x => x == '.') != 3 )
            {
                return false;
            }
            else
            {
                IPAddress address;
                var ipRes=IPAddress.TryParse(_text,out address);
                return ipRes;
            }
           
        }
    }
}