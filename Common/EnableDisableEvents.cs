﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class EnableDisableEvents : MonoBehaviour
{
    public UnityEvent EnableEvent;
    public UnityEvent DisableEvent;

    private void OnEnable()
    {
        EnableEvent.Invoke();
    }
    private void OnDisable()
    {
        DisableEvent.Invoke();
    }
}
