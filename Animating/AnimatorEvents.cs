﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace AbdLibrary
{
    public class AnimatorEvents : MonoBehaviour
    {
        public UnityEvent animatorEventForward, animatorEventBackward;
        public UnityEvent[] events;
        public UnityEvent[] animatorForwardEvents, animatorBackwardsEvents;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void CallMethodOnForwardAnimatorSpeed()
        {
            float speed = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).speed;
            if (speed >= 1)
            {
                animatorEventForward.Invoke();
            }
        }
        public void CallMethodOnForwardAnimatorSpeedEvents(int num)
        {
            float speed = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).speed;
            if (speed >= 1)
            {
                animatorForwardEvents[num].Invoke();
            }
        }
        public void CallMethodOnBackwarddAnimatorSpeed()
        {
            float speed = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).speed;
            if (speed < 0)
            {
                animatorEventBackward.Invoke();
            }
        }
        public void CallMethodOnBackwarddAnimatorSpeedEvents(int num)
        {
            float speed = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).speed;
            if (speed < 0)
            {
                animatorBackwardsEvents[num].Invoke();
            }
        }
        public void InvokeEvent(int num)
        {
            events[num].Invoke();
        }
    }
}
