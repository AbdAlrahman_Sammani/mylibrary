﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace AbdLibrary { 
public enum Type { Rotate,Scale,DegRotation,UIFade,OnActive}
    public class AnimationByScripts : MonoBehaviour
    {
        public float rotateSpeed = 10;
        public float scaleValue = 0.01f;
        public float _30degSpeed;
        public Type type = new Type();
        Vector3 localScaleValue;
        #region Control Parameter
        private bool activeAnimatingStart;
        private bool disactiveAnimatingStart;
        #endregion
        // Use this for initialization
        void Start()
        {
            localScaleValue = transform.localScale;
            if (type == Type.OnActive)
                transform.localScale = Vector3.zero;
        }
        private void OnEnable()
        {
            if (type == Type.OnActive)
            {
                CallActiveDisactive(true);
            }
            
        }
        private void OnDisable()
        {
            if (type == Type.OnActive)
            {
                disactiveAnimatingStart = false;
                activeAnimatingStart = false;
            }
        }
        // Update is called once per frame
        void Update()
        {
            if (type == Type.Rotate)
            {
                RotateAnimation();
            }
            else if (type == Type.Scale)
            {
                ScaleAnimation();
            }
            else if (type == Type.DegRotation)
            {
                Degree30Rotate();
            }
            else if (type == Type.UIFade)
            {
                FadeAnimation();
            }
            
           // AnimatingActive();
         //   AnimatingDisactive();
      
        }
        public void RotateAnimation()
        {
            transform.Rotate(Vector3.Lerp(transform.rotation.eulerAngles, new Vector3(0, 0, 1), rotateSpeed));
        }
        bool dec;
        public void ScaleAnimation()
        {
            if ((int)transform.localScale.magnitude < 20 && !dec)
            {
                transform.localScale += new Vector3(scaleValue, scaleValue, 1);
            }
            else
            {
                dec = true;
            }
            if (dec)
            {
                transform.localScale -= new Vector3(scaleValue, scaleValue, 1);
            }
            if ((int)transform.localScale.magnitude < 2)
            {
                dec = false;
            }
        }
        bool degDec = false;
        public void Degree30Rotate()
        {
            if (transform.rotation.eulerAngles.z >= 45)
            {
                degDec = true;
            }
            if (degDec == false)
            {
                transform.Rotate(new Vector3(0, 0, _30degSpeed * Time.deltaTime));
            }
            if (transform.localRotation.eulerAngles.z <= 10)
            {
                degDec = false;
            }
            if (degDec == true)
            {
                transform.Rotate(new Vector3(0, 0, -_30degSpeed * Time.deltaTime));
            }

        }
        bool colorDec = true;
        public void FadeAnimation()
        {
            Image im = GetComponent<Image>();
            Color A = new Color(im.color.r, im.color.g, im.color.b, 0.1f);
            Color B = new Color(im.color.r, im.color.g, im.color.b, 1);
            if (im.color.a >= 0.9f)
            {
                colorDec = true;
            }
            if (colorDec == true)
            {
                im.color = Color.Lerp(im.color, A, Mathf.PingPong(Time.deltaTime, 0.1f));
            }
            if (im.color.a <= 0.6f)
            {
                colorDec = false;
            }
            if (colorDec == false)
            {
                im.color = Color.Lerp(im.color, B, Mathf.PingPong(Time.deltaTime, 0.7f));
            }
        }
        public void AnimatingActive()
        {
            if (activeAnimatingStart)
            {
                gameObject.SetActive(true);

                if (activeAnimatingStart)
                {
                    if (gameObject.activeSelf == false)
                    {
                        gameObject.SetActive(true);
                    }
                    if (transform.localScale.magnitude < localScaleValue.magnitude)
                    {
                        transform.localScale = Vector3.MoveTowards(transform.localScale, localScaleValue, Time.deltaTime);
                    }
                    else
                    {
                     //   activeAnimatingStart = false;
                    }
                }
            }
        }
        public void AnimatingDisactive()
        {
            if (activeAnimatingStart==false)
            {
                if (transform.localScale.magnitude > Vector3.zero.magnitude)
                {
                    transform.localScale = Vector3.MoveTowards(transform.localScale, Vector3.zero, Time.deltaTime);
                }
                else if (transform.localScale.magnitude == Vector3.zero.magnitude)
                {
                    //  gameObject.SetActive(false);
                   // activeAnimatingStart = true;
                }
            }
        } 
        public void CallActiveDisactive(bool state)
        {
            if (state)
            {
                activeAnimatingStart = true;
            }
            else
            {
                activeAnimatingStart = false;
            }
        }

    }
}
