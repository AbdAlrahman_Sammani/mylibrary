﻿using UnityEngine;
using System.Collections;
/*
namespace AbdLibrary{
public class WebcamViewer1 : MonoBehaviour
{
    private class CamInstance
    {
        public string name;
        public WebCamTexture cam_texture;
    }
    public GameObject viewingBoard;
    private WebCamTexture webCamTex;
    private CamInstance[] camInstances;
    public Vector2 resloution;
    public int frameRate;
    public bool isActive;
    public int camIndex;
    public virtual void SetCameraIndex()
    {
        camIndex = 0;
    }
    // Use this for initialization
    public virtual void Start()
    {
        SetCameraIndex();
        int numCams = WebCamTexture.devices.Length;
        camInstances = new CamInstance[numCams];
        for (int i = 0; i < numCams; i++)
        {

            CamInstance instance = new CamInstance();
            instance.name = WebCamTexture.devices[i].name;


            camInstances[i] = instance;
        }
        StartWebcam(camIndex);
        StopWebCamera();
    }
    public void StartWebcam(int camID)
    {
        // NOTE: WebcamTexture can be slow for high resolutions, this can cause issues with audio-video sync.
        // Our plugins AVPro Live Camera or AVPro DeckLink can be used to capture high resolution devices
        camInstances[camID].cam_texture = new WebCamTexture(camInstances[camID].name, (int)resloution.x, (int)resloution.y, frameRate);

        camInstances[camID].cam_texture.Play();
        viewingBoard.GetComponent<MeshRenderer>().material.mainTexture = camInstances[camID].cam_texture;


    }
    public void StopWebCamera()
    {
        foreach (var item in camInstances)
        {
            item.cam_texture.Stop();

        }
    }
    public void PauseCamera()
    {
        foreach (var item in camInstances)
        {
            item.cam_texture.Pause();
        }
    }
    public void PlayCamera()
    {

        foreach (var item in camInstances)
        {
            item.cam_texture.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S)) StopWebCamera();
    }
    public void ToggleCamera(bool state)
    {
        isActive = state;
        if (isActive) camInstances[0].cam_texture.Play();
        else
        {
            if (camInstances[0].cam_texture.isPlaying)
                StopWebCamera();
        }
    }
}
}
*/