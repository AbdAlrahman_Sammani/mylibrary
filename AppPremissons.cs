using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class AppPremissons : MonoBehaviour
{
    public static bool checkPermissionOnFocus = false;
    public static bool IsCameraHasPermission()
    {
        return Permission.HasUserAuthorizedPermission(Permission.Camera);
    }
    public static bool CheckCameraPermission()
    {
#if UNITY_IOS
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera)) {
            // PERMISSION NOT AVAILABLE ON IOS, DISPLAY POPUP MESSAGE
                    UIManager.GetCameraPermissionUI().Show();

        }
#endif
#if UNITY_ANDROID
       // UIManager.GetCameraPermissionUI().Show();
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            checkPermissionOnFocus = true;
            Permission.RequestUserPermission(Permission.Camera);
            // PERMISSION NOT AVAILABLE ON ANDROID, DISPLAY POPUP MESSAGE
        }
        else
        {
            checkPermissionOnFocus = false;
        }
       
#endif

        return Permission.HasUserAuthorizedPermission(Permission.Camera);
    }

}
