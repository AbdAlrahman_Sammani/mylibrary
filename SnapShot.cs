﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AbdLibrary {
    public class SnapShot : MonoBehaviour
    {

        public Vector2 startPoint, endPoint;
        private Texture2D snapTexture;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public static IEnumerator TakeSnapShotandSaveToStreamingAsset(Vector2 startpos, Vector2 endpos, Camera camera,/*Vector2 snapSize,/* Texture2D tex*/string filename)
        {
            yield return new WaitForEndOfFrame();

            var tempTexture = RenderTexture.GetTemporary(Screen.width, Screen.height);
            var lastTarget = camera.targetTexture;
            camera.targetTexture = tempTexture;
            camera.Render();

            var lastActiveRenderTexture = RenderTexture.active;
            RenderTexture.active = tempTexture;
            Vector2 snapSize;
            startpos = camera.WorldToScreenPoint(startpos);
            endpos = camera.WorldToScreenPoint(endpos);
            snapSize = endpos - startpos; 
            //  tex = new Texture2D(((int)endpos.x - (int)startpos.x), ((int)endpos.y - (int)startpos.x), TextureFormat.ARGB32, false);
            Texture2D tex = new Texture2D((int)snapSize.x, (int)snapSize.y, TextureFormat.ARGB32, false);
            Rect myRect = new Rect(startpos, snapSize);
            //   Rect myRect = new Rect(Vector2.zero,new Vector2(Screen.width,Screen.height));
            tex.ReadPixels(myRect, 0, 0);
            tex.Apply();

            RenderTexture.active = lastActiveRenderTexture;
            camera.targetTexture = lastTarget;
            RenderTexture.ReleaseTemporary(tempTexture);

            // texToSavein = tex;
            //   texToSavein.Apply();
            SaveFileToStreamingAsset.SaveFileToStraming(tex.EncodeToPNG(), filename);
        }
    }
   
}
