﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
namespace AbdLibrary {
    public class ReadFiles : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public static byte[] ReadFileFromStreamingAssets(string pathinStreamingAsset)
        {
            pathinStreamingAsset = Application.streamingAssetsPath + pathinStreamingAsset;
            byte[] data = File.ReadAllBytes(pathinStreamingAsset);
            return data;
        }
       public static string ReadStringFromStreamingassets(string path)
        {
            string value = File.ReadAllText(Application.streamingAssetsPath + path);
            return value;
        }
    }

}
