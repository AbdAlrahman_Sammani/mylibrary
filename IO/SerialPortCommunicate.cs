﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;
using System.Threading;
using System;
using TMPro;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

public class SerialPortCommunicate : MonoBehaviour
{
    public enum SerialDataType { DataByte, StringData }
    public SerialDataType _serialDataType;
    [Serializable]
    public class SerialCommunicationProperties
    {
        public string COM;
        public int BaudRate;
        public int DataBits;
    }
    [SerializeField]
    public SerialCommunicationProperties _SerialProperties;


    public string _serialdataToSend;
    public string recievedData;
    public int bufferSize = 6;
    public byte[] recivedByte;

    public SerialPort _serialPort = new SerialPort();
    Thread serialThread;

    public Action<string> _OnDataRecieved;
    public Action<byte[]> _OnDataRecieved_Bytes;
    public bool testMode;
    public bool dataRecievedActionTrigger;
    public bool dataRecieved_Bytes_ActionTrigger;

    //public Button setSerialSettings;
    //public Button sendSerialData;

    // Start is called before the first frame update
    void Start()
    {

        // setSerialSettings.onClick.AddListener(() => OpenSerial());
        //  sendSerialData.onClick.AddListener(() => SendSerialData());

#if UNITY_EDITOR
        EditorApplication.playModeStateChanged += LogPlayModeState;
#endif


    }
    private void Update()
    {
        if (dataRecievedActionTrigger)
        {
            Debug.Log("Triggered");
            dataRecievedActionTrigger = false;
            _OnDataRecieved.Invoke(recievedData);
        }
        if (dataRecieved_Bytes_ActionTrigger)
        {
            Debug.Log("Triggered");
            dataRecieved_Bytes_ActionTrigger = false;
            _OnDataRecieved_Bytes.Invoke(recivedByte);
        }
    }
    public void StartSerialCommunication()
    {
        _serialPort.PortName = _SerialProperties.COM;
        _serialPort.BaudRate = _SerialProperties.BaudRate;
        _serialPort.DataBits = _SerialProperties.DataBits;

        OpenSerial();

        switch (_serialDataType)
        {
            case SerialDataType.DataByte:
                serialThread = new Thread(ReadSerialBytes);
                break;
            case SerialDataType.StringData:
                serialThread = new Thread(ReadSerial_String);
                break;
            default:
                break;
        }
        serialThread.Start();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            serialThread.Abort();
        }

        //   _serialPort.BaseStream.Flush();

    }
    public IEnumerator ReadSerial()
    {

        _serialPort.Open();
        yield return new WaitForEndOfFrame();
        while (true)
        {
            try
            {

                string dataToRead = _serialPort.ReadExisting();
                byte[] dataBuffer = ASCIIEncoding.Default.GetBytes(dataToRead);
                string value = ASCIIEncoding.Default.GetString(dataBuffer);

                // byte[] bb = new byte[16];
                //   _serialPort.Read(bb, 0, 0);
                //   Debug.Log("==================" + value);
                //  recievedData.text += value;
                recievedData = value;
                if (value != null)
                {
                    print("DATA=" + value);

                }
                else
                {
                    print("DATA=null");

                }

            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
            yield return new WaitForEndOfFrame();
        }
    }
    public void OnDataRecieved(object sender, SerialDataReceivedEventArgs e)
    {
        Debug.Log("DATA REC");
        SerialPort sp = (SerialPort)sender;
        string data = sp.ReadExisting();
        Debug.Log("DATA=" + data);
    }
    public string GetDataRecieved()
    {
        return recievedData;
    }

    public void OpenSerial()
    {

        try
        {
            _serialPort.PortName = _SerialProperties.COM;
            _serialPort.BaudRate = _SerialProperties.BaudRate;
            _serialPort.DataBits = _SerialProperties.DataBits;
            _serialPort.StopBits = StopBits.One;
            _serialPort.Parity = Parity.None;
            //  _serialPort.ReadTimeout = 1000;
            //   _serialPort.WriteTimeout = 1000;
            //     StartCoroutine(ReadSerial());
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(OnDataRecieved);
            _serialPort.Open();

            //_serialPort.Open();
        }
        catch (Exception e)
        {
            Debug.LogError("ERROR===" + e.Message);
            throw;
        }
        Debug.Log("Open");
        //if (_serialPort != null && _serialPort.IsOpen)
        //{
        //    _serialPort.Close();
        //}



    }
    public void ReadSerial_String()
    {
        while (true)
        {
            try
            {

                string dataToRead = _serialPort.ReadLine();
                byte[] dataBuffer = ASCIIEncoding.Default.GetBytes(dataToRead);
                string value = ASCIIEncoding.Default.GetString(dataBuffer);
                Debug.Log("VAL==" + value);
                // byte[] bb = new byte[16];
                //   _serialPort.Read(bb, 0, 0);
                //   Debug.Log("==================" + value);
                //  recievedData.text += value;
                recievedData = value;
                if (value.Length > 0)
                {

                    // 
                    dataRecievedActionTrigger = true;

                }
                else
                {
                    print("DATA=null");

                }

            }
            catch (IOException e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    public void ReadSerialBytes()
    {
        recivedByte = new byte[24];
        byte[] buffer = new byte[bufferSize];
        List<byte> incomeDataList = new List<byte>();
        while (true)
        {
            try
            {
                Thread.Sleep(500);

                // int size= _serialPort.BytesToRead;
                // Debug.Log("SIZE=" + size);
                // buffer = new byte[bufferSize];
                //   string dataToRead = _serialPort.ReadLine();
                int numberOfBytesToRead = _serialPort.BytesToRead;
             //   Debug.Log("SSS=" + numberOfBytesToRead);
                if (numberOfBytesToRead == 0) continue;
                recivedByte = new byte[numberOfBytesToRead];
                int bytesCount = _serialPort.Read(recivedByte, 0, recivedByte.Length);
               // Debug.Log("NUM=" + bytesCount);
                // var dd= _serialPort.ReadByte();
                // incomeDataList.Add(dd);
                //foreach (var item in recivedByte)
                //{
                //    Debug.Log("DATA===" + item);
                //}
                //Debug.Log("......");
                //  Thread.Sleep(1000);

                //  var t = _serialPort.ReadExisting();
                //  Debug.Log("EX==" + t);
                // byte[] dataBuffer = ASCIIEncoding.Default.GetBytes(dataToRead);
                // string value = ASCIIEncoding.Default.GetString(dataBuffer);

                // byte[] bb = new byte[16];
                //   _serialPort.Read(bb, 0, 0);
                //   Debug.Log("==================" + value);
                //  recievedData.text += value;
                //  recivedByte = new byte[buffer.Length];
                //recivedByte = buffer;


                if (numberOfBytesToRead == bytesCount && numberOfBytesToRead > 0)
                {
                  //  Debug.Log("TTT");
                    dataRecieved_Bytes_ActionTrigger = true;
                    //foreach (var item in recivedByte)
                    //{
                    //    Debug.Log("REC==" + item);
                    //}
                }
                else
                {
                    print("DATA=null");

                }


            }
            catch (IOException e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    public void SendSerialData(string data)
    {
        if (_serialPort.IsOpen)
        {
            string dataToSend = data;
            _serialPort.WriteLine(dataToSend);
            Debug.Log("Sent==" + dataToSend);
        }
    }
    public void SendSerialData(byte[] data)
    {
        if (_serialPort.IsOpen)
        {
            _serialPort.Write(data, 0, data.Length);
            Debug.Log("Sent==" + data[0]);
        }
    }
    private void OnApplicationQuit()
    {
        //   thr.Abort();
        if (serialThread.IsAlive)
            serialThread.Abort();

        _serialPort.Dispose();
        _serialPort.Close();
    }
    private void OnApplicationPause(bool pause)
    {
        Debug.Log("PAUSE=" + pause);
        //if (pause)
        //{
        //    _serialPort.Dispose();
        //    _serialPort.Close();
        //}
        //else
        //{
        //    OpenSerial();
        //}
    }


#if UNITY_EDITOR	
    private void LogPlayModeState(PlayModeStateChange state)
    {
        if (!Application.isEditor) return;
        Debug.Log(state);
        if (state == PlayModeStateChange.ExitingPlayMode)
        {
            serialThread.Abort();
        }
    }
#endif
    private void OnGUI()
    {
        if (testMode)
        {
            GUI.Label(new Rect(10, 10, 600, 300), recievedData);
            GUI.skin.label.fontSize = 150;
        }

    }


}
